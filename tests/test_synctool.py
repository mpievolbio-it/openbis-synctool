import os
import shutil
import unittest

from openbis_synctool.SyncTool import SyncTool


class SyncToolTest(unittest.TestCase):
    """
    Test class to host all test functions for the SyncTool class.
    """
    def setUp(self) -> None:
        """ Set up a test. """
        self._thrash = []

    def tearDown(self) -> None:
        """ Tear down a test."""

        # Empty thrash.
        for item in self._thrash:
            if os.path.isfile(item):
                os.remove(item)
            elif os.path.isdir(item):
                shutil.rmtree(item)

    def test_construction(self):
        """ Test constructing the SyncTool class. """

        synctool = SyncTool()
        self.assertIsInstance(synctool, SyncTool)


if __name__ == '__main__':
    unittest.main()
