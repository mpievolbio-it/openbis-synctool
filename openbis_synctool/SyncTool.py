""" :module SyncTool: This module hosts the SyncTool class and associated constants and functions."""

class SyncTool(object):
    """ :class SyncTool: This class implements the synchronization between two openbis instances. """

    pass